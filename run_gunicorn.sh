#!/bin/sh
export $(xargs < .env.dev)
gunicorn --bind 0.0.0.0:${APP_PORT} --workers 2 --worker-connections 5000 --timeout 6000 manage:app