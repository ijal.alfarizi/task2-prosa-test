import unittest
from app.services.converter_service import ConverterService
from app.models.converter_model import ConverterModel
import datetime
from mongoengine.errors import ValidationError


class ConverterServiceTest(unittest.TestCase):


    def setUp(self):
        self.service = ConverterService()

    def test_service(self):
        data = {
            'file_name' : 'test.mp4',
            'upload_date': datetime.datetime.now(),
            'convert_status': 'on_progress',
            'output_format': 'mov',
            'output_preset': ['out_presets']
        }
        # test insert
        try:
            saved = self.service.store(data)

        except Exception as e:
            # print(e)
            self.fail()
        
        find = self.service.get_by_id(str(saved.id))
        # test get by id
        self.assertNotEqual(find, None)

        # test update status
        self.assertNotEqual(self.service.update_status(saved.id, 'success'), None)

        # test update status fail
        with self.assertRaises(ValidationError) as ctx:
            self.service.update_status(saved.id, datetime.datetime.now())
        self.assertTrue(ctx)
        
        # test update status not found
        self.assertIsNone(self.service.update_status('5f69dc38925b7da3a7464039','success'))

        # test store none
        self.assertIsNone(self.service.store({}))

        fail_data = {
            'file_name' : 'test.mp4',
            'upload_date': datetime.datetime.now(),
            'convert_status': 'on_progress',
            'output_format': 'mov',
            'output_preset': 'out_presets'
        }

        # test error store
        with self.assertRaises(ValidationError) as ctx:
            self.service.store(fail_data)
        self.assertTrue(ctx)

   