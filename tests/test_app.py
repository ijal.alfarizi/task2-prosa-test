import unittest
import mongomock
import json
import datetime
from app import create_app
from flask import Flask
from unittest.mock import patch
from app.services.converter_service import ConverterService
import logging
import os
from io import StringIO
from werkzeug.datastructures import FileStorage
from app.utils.ffmpeg_converter import FFMPEGConverter
logging.basicConfig(level=logging.DEBUG)


class FFMPEGMock():
    def __init__(self, *args):
        logging.debug('Mocked')
        pass

    def convert(self, files):
        logging.debug('Mocked')
        pass


class OSMock():
    environ = {
            'MONGO_DB_HOST': 'mongomock://localhost',
            'MONGO_DB_PORT': '0',
            'MONGO_DB_USER': '',
            'MONGO_DB_PASSWORD': '',
            'APP_PORT': '5000',
            # 'STATIC_FOLDER': 'tests/static_files',
            'APP_FOLDER': 'tests',
        }

class AppTest(unittest.TestCase):
    def setUp(self):

        patch_environ = patch.dict('app.os.environ', values=OSMock().environ).start()
        # patch_environ.return_value = OSMock().environ
        self.test_app = create_app()
        self.test_app.config['STATIC_FOLDER'] = 'tests/static_files'
        self.client = self.test_app.test_client()
        self.service = ConverterService()
        self.headers = {
            'Accept': 'application/json',
            'Content-type': 'application/json'
        }


    def test_create_app(self):
        self.assertTrue(isinstance(self.test_app, Flask))


    def test_get_status(self):
        data = {
            'file_name' : 'test.mp4',
            'upload_date': datetime.datetime.now(),
            'convert_status': 'on_progress',
            'output_format': 'mov',
            'output_preset': ['out_presets']
        }
        saved = self.service.store(data)
        
        # test found
        req = self.client.get('/api/v1/video-converter/{0}/status'.format(str(saved.id)), headers= self.headers)
        res = json.loads(req.get_data(as_text=True))
        self.assertEqual(res['code'], 200)

        # test not found
        req = self.client.get('/api/v1/video-converter/5f6af1999b2ae52a3826cdda/status', headers= self.headers)
        res = json.loads(req.get_data(as_text=True))
        self.assertEqual(res['code'], 404)

        # test not recognize obid
        req = self.client.get('/api/v1/video-converter/5f6af1999b2ae52a382sad6cdda/status', headers= self.headers)
        res = json.loads(req.get_data(as_text=True))
        self.assertEqual(res['code'], 404)

        # test get success converted data
        data = {
            'file_name' : 'test.mp4',
            'upload_date': datetime.datetime.now(),
            'convert_status': 'success',
            'output_format': 'mov',
            'output_preset': ['out_presets']
        }
        saved = self.service.store(data)


        req = self.client.get('/api/v1/video-converter/{0}/status'.format(saved.id), headers=self.headers)
        res = json.loads(req.get_data(as_text=True))
        self.assertEqual(res['code'], 200)
        self.assertIsNotNone(res['data']['converted_file_url'])
    
    def test_convert(self):


        my_video = os.path.join("tests/static_files/video.mp4")
        
        my_file = FileStorage(
            stream=open(my_video, "rb"),
            filename="video.mp4",
            content_type="video/mpeg",
        )

        req = self.client.post('/api/v1/video-converter/?output_format=kjb&output_presets=fast', data={
            'file': my_file
        })
        res = json.loads(req.get_data(as_text=True))
     
        self.assertEqual(res['code'], 422)

        my_file2 = FileStorage(
            stream=open(my_video, "rb"),
            filename="video.mp4",
            content_type="video/mpeg",
        )

        req = self.client.post('/api/v1/video-converter/?output_format=mp4&output_presets=as', data={
            'file': my_file2
        }, content_type="multipart/form-data")
        res = json.loads(req.get_data(as_text=True))
        self.assertEqual(res['code'], 422)

        # test < 3 preset
        my_file3 = FileStorage(
            stream=open(my_video, "rb"),
            filename="video.mp4",
            content_type="video/mpeg",
        )

        req = self.client.post('/api/v1/video-converter/?output_format=mp4&output_presets=fast,slow', data={
            'file': my_file3
        }, content_type="multipart/form-data")
        res = json.loads(req.get_data(as_text=True))
        self.assertEqual(res['code'], 422)

        # test upload
        patch_converter = patch('app.apis.converter.views.FFMPEGConverter').start()
        patch_converter.return_value = FFMPEGMock
        my_file4 = FileStorage(
            stream=open(my_video, "rb"),
            filename="video.mp4",
            content_type="video/mpeg",
        )

        req = self.client.post('/api/v1/video-converter/?output_format=mp4&output_presets=fast,slow,veryfast', data={
            'file': my_file4
        }, content_type="multipart/form-data")
        res = json.loads(req.get_data(as_text=True))
        self.assertEqual(res['code'], 200)

        # error upload test
        self.test_app.config['STATIC_FOLDER'] = 'nonono'
        my_file5 = FileStorage(
            stream=open(my_video, "rb"),
            filename="video.mp4",
            content_type="video/mpeg",
        )

        req = self.client.post('/api/v1/video-converter/?output_format=mp4&output_presets=fast,slow,veryfast', data={
            'file': my_file5
        }, content_type="multipart/form-data")
        res = json.loads(req.get_data(as_text=True))
        self.assertEqual(res['code'], 400)

        folder_list = os.listdir('tests/static_files')
        for f in folder_list:
            if f != 'video.mp4':
                os.remove('tests/static_files/{0}'.format(f))
        
        patch_converter.stop()

    def test_ffmpeg_converter(self):
        data = {
            'file_name': 'video.mp4',
            'upload_date': datetime.datetime.now(),
            'convert_status': 'on_progress',
            'output_format': 'mov',
            'output_preset': ['fast', 'veryfast', 'ultrafast']
        }
        try:
            saved = self.service.store(data)
            converter = FFMPEGConverter(['fast', 'veryfast', 'ultrafast'], 'mp4', saved.id)

            converter.convert('data/video.mp4')
        except Exception as e:
            logging.debug(e)
            self.fail()
        

if __name__ == "__main__":
    unittest.main()