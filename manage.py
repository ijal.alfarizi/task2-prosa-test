from flask.cli import FlaskGroup

from bin.run import app


cli = FlaskGroup(app)


if __name__ == "__main__":
    cli()
