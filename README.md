## **FFMPEG Video Converter**

This project is web service app for converting video using ffmpeg lib.
this project is using `lib264` encoder that support `'mkv', 'mp4', ' avi', 'wmv', 'mpg', 'mov', 'flv'` format and `'ultrafast', 'superfast', 'veryfast', 'faster', 'fast', 'medium', 'slow', 'slower', 'veryslow', 'placebo'` preset according to [this description.](https://trac.ffmpeg.org/wiki/Encode/H.264)

To start this program there are 3 methods:

 1. Run via flask : run bash script `./run_flask.sh`
 2. Run via gunicorn : run bash script `./run_gunicorn.sh`

Converted file will be in `static_folder`
 
two methods above are using `.env.dev` file for environtment variable.

**NB: make sure you've installed ffmpeg lib in your computer**

run via docker method:
run `docker-compose -f docker-compose.yml up -d --build` 

this method is using `nginx` and port 5001 by default, if you want to change the port, first change `APP_PORT` variable in `.env.prod` and then change nginx mapping PORT in `docker-compose.yml` change only 5001 number.

if you run app via docker and start converting video it will give response url for check conversion status. if succeed, the url will give response list of converted video's url you can access. 

if you want to pull image from docker registry use `docker pull ijalalfrz/prosa-task2` and use `docker-compose.reg.yml` to build and run, make sure all config file in `docker-compose.reg.yml` is available (.env.prod, nginx config)

**NB: make sure port 27017 is not being used by other app, because docker need that port for MongoDB engine**

if you want to run unittest: run bash script `./run_test.sh`

for API testing and documentation is in : `/api/v1/` route, there will be Swagger UI for testing the API.