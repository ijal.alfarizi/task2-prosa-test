import logging
import datetime
import threading

from flask import jsonify, make_response
from flask import current_app as app
from flask_restx import Resource, Namespace
from werkzeug.datastructures import FileStorage
from ...services.converter_service import ConverterService
from ...utils.ffmpeg_converter import FFMPEGConverter
from ...utils.format_list import FORMAT, PRESET

logging.basicConfig(level=logging.DEBUG)

# init service and namespace
service = ConverterService()
api = Namespace('Video Converter',
                description='Convert video to ffmpeg video format')

# add file parser to endpoint
upload_parser = api.parser()
upload_parser.add_argument('file', location='files',
                           type=FileStorage, required=True)
upload_parser.add_argument('output_format', required=True)
upload_parser.add_argument('output_presets', required=True)


@api.route('/')
class ConverterResource(Resource):

    @api.expect(upload_parser)
    @api.doc('Submit video')
    def post(self):
        # logging.debug(request.files)

        args = upload_parser.parse_args()

        # validation data process
        video_file = args['file']
        file_type = video_file.filename.split('.')[1]
        if not video_file:
            return make_response(jsonify({
                'code': 422,
                'message': 'file not found'
            }), 422)

        if file_type not in FORMAT or args['output_format'] not in FORMAT:
            format_str = ''
            for f in FORMAT:
                format_str += ' {0}'.format(f)

            return make_response(jsonify({
                'code': 422,
                'message': 'Only{0} format supported'.format(format_str)
            }), 422)

        out_presets = args['output_presets'].split(',')
        contain_preset = any(elem in PRESET for elem in out_presets)

        if not contain_preset:
            preset_str = ''
            for p in PRESET:
                preset_str += ' {0}'.format(p)

            return make_response(jsonify({
                'code': 422,
                'message': 'Only{0} preset supported'.format(preset_str)
            }), 422)

        check_duplicate = list(dict.fromkeys(out_presets))
        if len(out_presets) < 3 or len(check_duplicate) < 3:
            return make_response(jsonify({
                'code': 422,
                'message': ('Preset must be three, separated with "," and make'
                            'sure there is no duplicate preset')
            }), 422)

        # upload and process data
        try:
            file_name = datetime.datetime.now().strftime("%m%d%Y%H%M%S%f")
            logging.debug(video_file)

            new_name = file_name + '.' + file_type

            folder = app.config['STATIC_FOLDER']

            video_file.save(folder + '/' + new_name)

        except Exception as e:
            logging.debug(e)
            return make_response(jsonify({
                'code': 400,
                'message': 'Error upload file'
            }), 400)

        try:
            data = {
                'file_name': new_name,
                'upload_date': datetime.datetime.now(),
                'convert_status': 'on_progress',
                'output_format': args['output_format'],
                'output_preset': out_presets
            }

            data = service.store(data)

            converter = FFMPEGConverter(out_presets, args['output_format'],
                                        str(data.id))

            # run as background job (non blocking)
            t = threading.Thread(target=converter.convert, args=(new_name,))
            t.daemon = True
            t.start()
            # converter.convert(new_name)

            port = app.config['APP_PORT']
            return make_response(jsonify({
                'code': 200,
                'message': ('Your file is being converted, check for the'
                            'status in this url'),
                'url': ('http://localhost:{0}/video_converter/{1}/'
                        'status'.format(port, str(data.id)))
            }), 200)

        except Exception as e:
            logging.debug(e)
            return make_response(jsonify({
                'code': 400,
                'message': 'Failed to save data'
            }), 400)


@api.route('/<id>/status')
@api.param('id', 'String mongodb object id')
@api.response(500, 'System error.')
@api.response(404, 'Data not found.')
class ConverterWebHook(Resource):

    @api.doc('Get status')
    @api.response(200, 'Data found.')
    def get(self, id):
        try:
            data = service.get_by_id(id)
        except Exception as e:
            logging.debug(e)
            return make_response(jsonify({
                'code': 404,
                'message': 'Data not found'
            }), 404)

        if data:
            file_name = data['file_name'].split('.')[0]
            port = app.config['APP_PORT']
            if data['convert_status'] != 'success':
                return make_response(jsonify({
                    'code': 200,
                    'data': {
                        'status': data['convert_status'],
                    }
                }), 200)
            else:
                url = {}
                op = data['output_format']
                for p in data['output_preset']:
                    url['{0}-url'.format(p)] = ('http://localhost:{0}'
                                                '/static/{1}_{2}.'
                                                '{3}'.format(port,
                                                             file_name,
                                                             p,
                                                             op))
                return make_response(jsonify({
                    'code': 200,
                    'data': {
                        'status': data['convert_status'],
                        'converted_file_url': url
                    }
                }), 200)
        else:

            return make_response(jsonify({
                'code': 404,
                'message': 'Data not found'
            }), 404)
