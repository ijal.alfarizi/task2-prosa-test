
from flask import Blueprint
from flask_restx import Api
# controller
from .converter.views import api as video_converter_ns


def generete_api():
    blueprint = Blueprint('api', __name__)
    api = Api(blueprint, title="Task 2", version="0.0.1",
              description="API for video converter")

    api.add_namespace(video_converter_ns, path='/video-converter')
    return blueprint
