FORMAT = ['mkv', 'mp4', ' avi', 'wmv', 'mpg', 'mov', 'flv']
PRESET = [
            'ultrafast',
            'superfast',
            'veryfast',
            'faster',
            'fast',
            'medium',
            'slow',
            'slower',
            'veryslow',
            'placebo'
        ]
