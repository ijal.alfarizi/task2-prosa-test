import subprocess
import logging
from datetime import timedelta
from thespian.actors import ActorSystem, ActorExitRequest, Actor
from .timer import timeit
from thespian.troupe import troupe
from ..services.converter_service import ConverterService
logging.basicConfig(level=logging.DEBUG)


class FFMPEGConverter():

    def __init__(self, out_preset, out_format, convert_id):

        self.service = ConverterService()
        self.convert_id = convert_id
        self.out_preset = out_preset
        self.out_format = out_format

    @timeit
    def convert(self, files):

        system = ActorSystem('multiprocQueueBase')
        ffmpeg_actor = system.createActor(FFMPEGActor)
        max_wait = timedelta(seconds=60)
        counter = 0
        for pre in self.out_preset:
            message = (pre, self.out_format, files)
            system.tell(ffmpeg_actor, message)

        for r in range(len(self.out_preset)):
            if system.listen(max_wait) == 'done':
                counter += 1

        logging.debug(counter)
        if counter == 3:
            self.service.update_status(self.convert_id, 'success')
        else:
            self.service.update_status(self.convert_id, 'failed')

        system.tell(ffmpeg_actor, ActorExitRequest())
        system.shutdown()


@troupe()
class FFMPEGActor(Actor):
    def __init__(self, *args, **kwargs):
        self.count = 0
        super().__init__(*args, **kwargs)

    def receiveMessage(self, message, sender):

        static_folder = 'static_files'
        preset = message[0]
        out_format = message[1]
        file_path = static_folder + '/' + message[2]
        file_name = (static_folder + '/' + message[2].split('.')[0] + '_'
                     + preset)

        cmd_process = "ffmpeg -i {0} -c:v libx264 -preset {1} \
                        -crf 10 {2}.{3}".format(file_path, preset,
                                                file_name, out_format)

        subprocess.call(cmd_process, shell=True)

        self.send(sender, 'done')


# class ExcutorActor(Actor):

#     def receiveMessage(self, message, sender):
#         logging.debug('a')
#         original_sender, cmd_process = message
#         subprocess.call(cmd_process)

#         self.send(original_sender, 'done')
