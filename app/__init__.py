from flask import Flask, send_from_directory
from flask_mongoengine import MongoEngine
from .apis import generete_api
import os
from werkzeug.middleware.proxy_fix import ProxyFix
dbmongo = MongoEngine()


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1, x_port=1, x_for=1,
                            x_host=1, x_prefix=1)

    print(os.environ['MONGO_DB_HOST'])

    app.config['MONGODB_SETTINGS'] = {
        'db': 'task2',
        'host': os.environ['MONGO_DB_HOST'],
        'port': int(os.environ['MONGO_DB_PORT']),
        'username': os.environ['MONGO_DB_USER'],
        'password': os.environ['MONGO_DB_PASSWORD']
    }

    app.config['APP_PORT'] = os.environ['APP_PORT']
    app.config['STATIC_FOLDER'] = os.environ['APP_FOLDER'] + '/static_files'

    dbmongo.init_app(app)

    api_bp = generete_api()
    app.register_blueprint(api_bp, url_prefix='/api/v1')

    @app.route("/static/<path:filename>")
    def staticfiles(filename):
        return send_from_directory(app.config["STATIC_FOLDER"], filename)

    if not os.path.isdir('./static_files'):
        os.mkdir('./static_files')
    return app
