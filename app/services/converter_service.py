from ..models.converter_model import ConverterModel


class ConverterService():

    def store(self, data):
        try:
            if not data:
                return None

            data = ConverterModel(**data)
            data.save()

            return data
        except Exception as e:

            raise e

    def get_by_id(self, id):

        data = ConverterModel.objects(id=id).first()

        return data

    def update_status(self, id, status):

        data = self.get_by_id(id)
        try:
            if data:
                data['convert_status'] = status
                data.save()
                return data
            else:
                return None
        except Exception as e:

            raise e
