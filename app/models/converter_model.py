from mongoengine import Document, StringField, DateTimeField, ListField


class ConverterModel(Document):

    file_name = StringField()
    upload_date = DateTimeField()
    convert_status = StringField(required=True)
    output_format = StringField(max_length=20)
    output_preset = ListField(StringField(max_length=20))
